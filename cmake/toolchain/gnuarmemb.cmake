set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(data_flags "-ffunction-sections -fdata-sections -fno-strict-aliasing -fno-builtin --short-enums")
set(warning_flags "-Wall -Wno-attributes -Wno-format")
set(CMAKE_C_FLAGS_INIT "${warning_flags} ${data_flags}")

set(cortex-m0_DEFINES
  -mcpu=cortex-m0
  -mthumb
  -mabi=aapcs
  -mfloat-abi=soft)

set(cortex-m4_DEFINES
  -mcpu=cortex-m4
  -mthumb
  -mabi=aapcs
  -mfloat-abi=soft)

set(cortex-m4f_DEFINES
  -mcpu=cortex-m4
  -mthumb
  -mabi=aapcs
  -mfloat-abi=hard
  -mfpu=fpv4-sp-d16)

function(add_hexfile TARGETNAME)
  add_custom_command(
    TARGET ${TARGETNAME}
    POST_BUILD
    COMMAND arm-none-eabi-objcopy -O ihex ${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.elf ${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.hex
    BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.hex)

  add_custom_command(
    TARGET ${TARGETNAME}
    POST_BUILD
    COMMAND arm-none-eabi-size ${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.elf)
endfunction(add_hexfile)

function(generate_linker_file TARGET)
  configure_file(${CMAKE_SOURCE_DIR}/cmake/linker/nrf5x.ld.in
    ${CMAKE_CURRENT_BINARY_DIR}/${PLATFORM}.ld)

  get_filename_component(__target_name
    ${TARGET}
    NAME_WE)

  set(__link_flags
    ${${${PLATFORM}_ARCH}_DEFINES}
    "-Wl,--gc-sections"
    "-T\"${CMAKE_CURRENT_BINARY_DIR}/${PLATFORM}.ld\""
    "-Xlinker -Map=\"${CMAKE_CURRENT_BINARY_DIR}/${__target_name}.map\""
    "-L\"${NRFX_PATH}/mdk\"")
  message(Link flags: ${__link_flags})
  string(REGEX REPLACE ";" " " __link_flags "${__link_flags}")
  set_target_properties(${TARGET} PROPERTIES LINK_FLAGS ${__link_flags})
endfunction(generate_linker_file)
