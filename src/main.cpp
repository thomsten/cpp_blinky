#include "nrf.h"
#include "nrf_gpio.h"
#include "core_cmFunc.h"

#define NOP_PER_MS 64000000

class Led {
public:
    Led(uint32_t led_pin) : pin(led_pin) {nrf_gpio_cfg_output(pin);};

    void on(void) {nrf_gpio_pin_clear(this->pin);};
    void off(void) {nrf_gpio_pin_set(this->pin);};
    void toggle(void) {nrf_gpio_pin_toggle(this->pin);};

private:
    const uint32_t pin;
};

static Led m_led = Led(21);

static void delay(uint32_t delay_ms)
{
    for (uint32_t i = 0; i < delay_ms; ++i)
        for (uint32_t i = 0; i < NOP_PER_MS; ++i)
            __NOP();
}

int main(void)
{
    nrf_gpio_cfg_output(22);
    while (1)
    {
        nrf_gpio_pin_toggle(22);
        m_led.toggle();
        delay(100);
    }
}
